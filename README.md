# Motors for STMF767ZI

Motor class is a representation of a single motor, its member 'compareRegister_' is a reference to memory-mapped **capture/compare register** of a corresponding timer's PWM channel.

All the functions together with the class are enclosed in **mtr** namespace.

# Adding to a project:
1. Include "Motor.hpp" to your main file.
2. Create instances of Motor class - third parameter in the constructor is the frequency of "APB1 timer clocks"/"APB2 timer clocks" - depending on the timer used and which bus it's connected to. (see **APBX bus connections.pdf**)





