/*
 * Motor.hpp
 *
 *  Created on: Aug 29, 2021
 *      Author: Maciej Gajek
 */

#ifndef _MOTOR_HPP_
#define _MOTOR_HPP_

#include <main.hpp>

namespace kn_robocik
{
namespace mtr
{

using TIM_TypeDef_CCRX = __IO uint32_t;

enum class TimerChannel
{
	ChannelOne = 1,
	ChannelTwo,
	ChannelThree,
	ChannelFour
//	ChannelFive,
//	ChannelSix
};

struct ElectronicSpeedController
{
	ElectronicSpeedController(
		const uint16_t reverseMaxSpeedMicroPW = 1100,
		const uint16_t motorStoppedMicroPW = 1500,
		const uint16_t forwardMaxSpeedMicroPW = 1900,
		const uint16_t maxUpdateHzFreq = 400);

	//Pulse width (PW) [us]
	const uint16_t reverseMaxSpeedMicroPW;
	const uint16_t motorStoppedMicroPW;
	const uint16_t forwardMaxSpeedMicroPW;

	const uint16_t pulseWidthMicroAmplitude;

	//Max update frequency [Hz]
	const uint16_t maxUpdateHzFreq;
};

class Motor
{
public:
	Motor(
		TIM_HandleTypeDef& timerHandle,
		const TimerChannel timerChannel,
		const uint32_t apbxTimerClockFreq,
		const ElectronicSpeedController& speedController = {},
		const int8_t transoptorOffDelay = -25);

	inline void stop();
	void setSpeed(const int8_t& speedPercentage);

private:
	void configureTimer();

	//Timer input frequency
	const uint32_t APBX_TIMER_CLOCK_HZ_FREQ;
	//We want one timer tick every microsecond to directly match ESC values
	static const uint32_t DESIRED_TICK_HZ_FREQ = 1000000;
	//Switch off delay in microseconds of transoptors
	const int8_t TRANSOPTOR_OFF_MICRO_DELAY;

	TIM_HandleTypeDef& timerHandle_;
	TIM_TypeDef_CCRX& compareRegister_;
	const ElectronicSpeedController speedController_;
	bool isInitialized_ = false;
};

}	// namespace mtr
}	// namespace kn_robocik

#endif /* _MOTOR_HPP_ */
