/*
 * Motor.cpp
 *
 *  Created on: Aug 29, 2021
 *      Author: Maciej Gajek
 */

#include <algorithm>
#include <stdexcept>

#include "Motor.hpp"

namespace kn_robocik
{
namespace mtr
{
namespace
{

TIM_TypeDef_CCRX* getChannelPtr(
	const TIM_HandleTypeDef& timerHandle,
	const TimerChannel timerChannel)
{
	TIM_TypeDef_CCRX* compareRegister;

	switch (timerChannel)
	{
	case TimerChannel::ChannelOne:
		compareRegister = &timerHandle.Instance->CCR1;
		break;
	case TimerChannel::ChannelTwo:
		compareRegister = &timerHandle.Instance->CCR2;
		break;
	case TimerChannel::ChannelThree:
		compareRegister = &timerHandle.Instance->CCR3;
		break;
	case TimerChannel::ChannelFour:
		compareRegister = &timerHandle.Instance->CCR4;
		break;
	default:
		throw std::invalid_argument("Wrong channel picked");
	}

	return compareRegister;
}

} // namespace

ElectronicSpeedController::ElectronicSpeedController(
	const uint16_t reverseMaxSpeedMicroPW,
	const uint16_t motorStoppedMicroPW,
	const uint16_t forwardMaxSpeedMicroPW,
	const uint16_t maxUpdateHzFreq)
	: reverseMaxSpeedMicroPW(reverseMaxSpeedMicroPW)
	, motorStoppedMicroPW(motorStoppedMicroPW)
	, forwardMaxSpeedMicroPW(forwardMaxSpeedMicroPW)
	, pulseWidthMicroAmplitude(forwardMaxSpeedMicroPW > motorStoppedMicroPW ?
			forwardMaxSpeedMicroPW - motorStoppedMicroPW : motorStoppedMicroPW - forwardMaxSpeedMicroPW)
	, maxUpdateHzFreq(maxUpdateHzFreq)
{
	assert(forwardMaxSpeedMicroPW - motorStoppedMicroPW == motorStoppedMicroPW - reverseMaxSpeedMicroPW);
	assert(forwardMaxSpeedMicroPW > reverseMaxSpeedMicroPW);
}

Motor::Motor(
	TIM_HandleTypeDef& timerHandle,
	const TimerChannel timerChannel,
	const uint32_t apbxTimerClockFreq,
	const ElectronicSpeedController& speedController,
	const int8_t transoptorOffDelay)
	: APBX_TIMER_CLOCK_HZ_FREQ(apbxTimerClockFreq)
	, TRANSOPTOR_OFF_MICRO_DELAY(transoptorOffDelay)
	, timerHandle_(timerHandle)
	, compareRegister_(*getChannelPtr(timerHandle, timerChannel))
	, speedController_(speedController)
{}

inline void Motor::stop()
{
	if (not isInitialized_)
	{
		configureTimer();
	}

	compareRegister_ = speedController_.motorStoppedMicroPW + TRANSOPTOR_OFF_MICRO_DELAY;
}

void Motor::setSpeed(const int8_t& speedPercentage)
{
	if (not isInitialized_)
	{
		configureTimer();
	}

	constexpr uint8_t hundredpercent = 100;
	// percentage [-100, 100] scaled to steering value
	uint32_t registerValue = speedPercentage * speedController_.pulseWidthMicroAmplitude / hundredpercent;

	// middle value of pulse width + steering value + transoptor delay
	registerValue = speedController_.motorStoppedMicroPW + registerValue + TRANSOPTOR_OFF_MICRO_DELAY;

	// limit checking
	registerValue = std::min(registerValue, static_cast<uint32_t>(speedController_.reverseMaxSpeedMicroPW));
	registerValue = std::max(registerValue, static_cast<uint32_t>(speedController_.forwardMaxSpeedMicroPW));

	compareRegister_ = registerValue;
}

void Motor::configureTimer()
{
	timerHandle_.Init.CounterMode = TIM_COUNTERMODE_UP;
	timerHandle_.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	timerHandle_.Init.RepetitionCounter = 0;
	timerHandle_.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;

	timerHandle_.Init.Prescaler = APBX_TIMER_CLOCK_HZ_FREQ/DESIRED_TICK_HZ_FREQ - 1;

	//Period value must be big enough for the update frequency to not exceed speedController_.maxUpdateFreq
	uint32_t newPeriodValue = DESIRED_TICK_HZ_FREQ/speedController_.maxUpdateHzFreq - 1;

	//Period value can't be lower than speedController_.forwardMaxSpeedPW
	newPeriodValue = std::max(newPeriodValue, static_cast<uint32_t>(speedController_.forwardMaxSpeedMicroPW));

	//Some other motor might have already set the period value so it can't be lowered
	timerHandle_.Init.Period = std::max(timerHandle_.Init.Period, newPeriodValue);

	if (HAL_TIM_Base_Init(&timerHandle_) != HAL_OK or HAL_TIM_Base_Start(&timerHandle_) != HAL_OK)
	{
		Error_Handler();
		return;
	}
}

} // namespace mtr
}	// namespace kn_robocik
